var request = require('request');
var fs = require('fs');
var Jimp = require("jimp");
var sizeOf = require('image-size');
var express = require('express')
var app = express();
var url = require("url");
var redis = require("redis");
var redis_client = redis.createClient();

var uriHasProtocol = (uri) => {
    var path = url.parse(uri);
    return !!path.protocol;
};

var createImagePath = (uri) => {
    var url_info = url.parse(uri);
    var host = url_info.host.replace("www.", "").replace(/\//g, "_").replace(/\\/g, "_");
    var path = url_info.path.replace(/\//g, "").replace(/\\/g, "_");
    return {
        host: host,
        path: path,
    };
};

var errorResponse = (res, msg, status) => {
    res.status(status || 400).send(msg || "Error during processing").end();
}

var getImageNames = (host, path, width, height) => {

};

if (!fs.existsSync("pictures")){
    fs.mkdirSync("pictures");
}

var pictureObjectExists = (key, callback) => {
    redis_client.get(key, (err, value) => {
        callback(value);
    });
};

var thumbExists = (obj, size) => {
    return obj.resized_size.indexOf(size) >= 0;  
};

var createThumb = (buffer, path, size_obj, file_path, callback) => {
    Jimp.read(buffer || file_path).then((thumb) => {
        thumb.resize(size_obj.width, size_obj.height)            // resize 
            .write(path, function (err, image) {
                callback(null, path);
            });
    }).catch((err) => {
        console.error(err);
        callback(err, null);
    });
};

var getOrigPicture = (uri, res, redis_obj, path, callback) => {
    var chunks = [];
    request.get(uri)
        .on('error', (err) => {
            console.log(err)
            errorResponse(res, 'bad url');
        })
        .on("response", (response) => {
            res.type(response.headers['content-type']); 
        })
        .on("data", (data) => {
            chunks.push(data);
        })
        .on("end", () => {
            var buffer = Buffer.concat(chunks);
            var size = sizeOf(buffer);
            redis_obj = {
                url: uri,
                width: size.width,
                height: size.height,
                file_name: uri.split('/').pop(),
                resized_size: [],
            }
            var stream = fs.createWriteStream(path);
            stream.once('open', function(fd) {
                stream.write(buffer);
                stream.end();
                redis_client.set(uri, JSON.stringify(redis_obj));
                callback(null, buffer, redis_obj);
            });
        });
};

var checkSize = (size_obj, redis_obj) => {
    var orig_scale = redis_obj.width / redis_obj.height;
    var thumb_scale;
    
    if (size_obj.width === -1) {
        size_obj.width = size_obj.height * orig_scale;
    } else if (size_obj.height === -1) {
        size_obj.height = size_obj.width / orig_scale;
    }
    
    thumb_scale = size_obj.width / size_obj.height;
    
    if (thumb_scale < orig_scale) {
        size_obj.height = size_obj.width / orig_scale;
    } else if (thumb_scale > orig_scale) {
        size_obj.width = size_obj.height * orig_scale;
    }
};

var resize = (uri, width, height, res) => {
    var size_obj = {
        width: width,
        height: height,
    };
    var redis_obj = {};
    var orig_path;
    var obj = createImagePath(uri);
    var thumb_path = "pictures/" + obj.host + "/" + obj.path + "_";
    var extension = uri.split('.').pop();
    if (!fs.existsSync("pictures/" + obj.host)){
        fs.mkdirSync("pictures/" + obj.host);
    }
    orig_path = "pictures/" + obj.host + "/" + obj.path;
    pictureObjectExists(uri, (value) => {
        if (value) {
            redis_obj = JSON.parse(value);
            console.log("found orig picture from redis");
            checkSize(size_obj, redis_obj);
            thumb_path +=  size_obj.width + "x" + size_obj.height + "." + extension;
            if (thumbExists(redis_obj, (size_obj.width + "x" + size_obj.height))) {
                console.log("Thumb exists, returning existing file");
                fs.createReadStream(thumb_path).pipe(res);
            } else {
                createThumb(null, thumb_path, size_obj, orig_path, (err, path) => {
                    console.log("create new thumb from existing orig");
                    if (err) {
                        errorResponse(res, err);
                    } else {
                        redis_obj.resized_size.push(size_obj.width + 'x' + size_obj.height);
                        redis_client.set(uri, JSON.stringify(redis_obj));
                        fs.createReadStream(thumb_path).pipe(res);
                    }
                });
            }
        }  else {
            getOrigPicture(uri, res, redis_obj, orig_path, (err, buffer, hmm) => {
                if (err) {
                    errorResponse(res, err);
                } else {
                    checkSize(size_obj, hmm);
                    thumb_path +=  size_obj.width + "x" + size_obj.height + "." + extension;
                    createThumb(buffer, thumb_path, size_obj, null, (err, path) => {
                        if (err) {
                            errorResponse(res, err);
                        } else {
                            hmm.resized_size.push(size_obj.width + 'x' + size_obj.height);
                            redis_client.set(uri, JSON.stringify(hmm));
                            fs.createReadStream(thumb_path).pipe(res)
                        }
                    });
                }
            });
        }
    });
};

app.get("/resize", (req, res) => {
    var uri = req.query.url;
    var width = req.query.width ? parseInt(req.query.width, 10) : Jimp.AUTO;
    var height = req.query.height ? parseInt(req.query.height, 10) : Jimp.AUTO;

    if (uriHasProtocol(uri)) {
        resize(uri, width, height, res);
    } else {
        errorResponse(res, 'Given url is missing protocol (http/https)');
    }
});

app.listen(process.env.PORT, function () {
    console.log('Example app listening on port ' + process.env.PORT + '!')
})
